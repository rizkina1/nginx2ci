#From
FROM rizkinug/testnginx:main

#COPY index.php /var/www/html/index.php
#Apply Web SSL
COPY web-ndr-ssl /etc/nginx/sites-enabled/default

#Hapus Script Lama
RUN rm /var/www/html/index.php
RUN rm /var/www/html/inpo.php


#Info PHP
COPY ci443/ /var/www/html/

#Apabila pakai CI
RUN chmod -R 0755 /var/www/html/writable
RUN chown -R www-data:www-data /var/www/html/writable

